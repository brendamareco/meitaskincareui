package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import views.form.ReportForm;

public class SelectFileController implements ActionListener 
{
	private ReportForm form;
	
	public SelectFileController(ReportForm form)
	{
		this.form = form;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		int fileChooserValue = form.getFileChooser().showOpenDialog(form);
		if (fileChooserValue == JFileChooser.APPROVE_OPTION)
		{
			File file = form.getFileChooser().getSelectedFile();
			form.getImgPathTxtField().setText(file.getAbsolutePath());
		}
	}
}
