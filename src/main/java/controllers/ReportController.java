package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import domain.data.CompatibilityReport;
import useCase.MakeReportUseCase;
import views.form.ReportForm;
import views.report.ReportView;

public class ReportController implements ActionListener
{
	private ReportForm form;
	private MakeReportUseCase makeReportUseCase;
	
	public ReportController(ReportForm form, MakeReportUseCase makeReportUseCase)
	{
		this.form = form;
		this.makeReportUseCase = makeReportUseCase;
		form.getGenerateReportBtn().addActionListener(this);
	}

	public void start()
	{
		form.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String imgPath = form.getImgPathTxtField().getText();
		CompatibilityReport report = makeReportUseCase.makeReport(imgPath);
		ReportView reportView = new ReportView(report);
		reportView.setVisible(true);
	}
}
