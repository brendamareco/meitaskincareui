package views.report;

import java.awt.Color;
import java.awt.Font;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import domain.data.CompatibilityReport;

public class ReportView extends JFrame
{
	private static final long serialVersionUID = 1L;
	private CompatibilityReport compatibilityReport;
	
	public ReportView(CompatibilityReport compatibilityReport)
	{
		this.compatibilityReport = compatibilityReport;
		initialize();
	}
	
	private void initialize() 
	{
		setTitle("Meita SkinCare 1.0");
		setBounds(100, 100, 614, 464);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JLabel isCompatibleLbl = new JLabel("");
		isCompatibleLbl.setHorizontalAlignment(SwingConstants.CENTER);
		isCompatibleLbl.setFont(new Font("Tahoma", Font.PLAIN, 12));
		isCompatibleLbl.setBounds(20, 171, 551, 36);
		
		if (compatibilityReport.isCompatible())
			isCompatibleLbl.setText("El cosm\u00E9tico es compatible para su uso");
		else
			isCompatibleLbl.setText("El cosm\u00E9tico no es compatible para su uso");
		getContentPane().add(isCompatibleLbl);
		
		JPanel compatibleIngrsContainer = new JPanel();
		compatibleIngrsContainer.setLayout(null);
		compatibleIngrsContainer.setBounds(20, 218, 551, 196);
		getContentPane().add(compatibleIngrsContainer);
		
		JScrollPane compatibleScrollPane = new JScrollPane();
		compatibleScrollPane.setBounds(10, 11, 250, 174);
		compatibleIngrsContainer.add(compatibleScrollPane);
		
		JLabel compatibleIngrsLbl = new JLabel("Ingredientes compatibles");
		compatibleIngrsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		compatibleScrollPane.setColumnHeaderView(compatibleIngrsLbl);
		
		JList<String> compatibleList = new JList<String>();
		compatibleScrollPane.setViewportView(compatibleList);
		Set<String> compatibleValues = new HashSet<String>();
		Set<String> incompatibleValues = new HashSet<String>();
		compatibilityReport.getCompatibilityPerIngrs().forEach((key, value) -> 
		{
		    if (value)
		        compatibleValues.add(key);
		    else
		    	incompatibleValues.add(key);
		});
		compatibleList.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = 1L;
			String[] values = compatibleValues.toArray(new String[0]);
			public int getSize() {return values.length;}
			public String getElementAt(int index) {return values[index];}
		});
		
		JScrollPane incompatibleScrollPane = new JScrollPane();
		incompatibleScrollPane.setBounds(291, 11, 250, 174);
		compatibleIngrsContainer.add(incompatibleScrollPane);
		
		JLabel incompatibleIngrsLbl = new JLabel("Ingredientes incompatibles");
		incompatibleIngrsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		incompatibleScrollPane.setColumnHeaderView(incompatibleIngrsLbl);
		
		JList<String> incompatibleList = new JList<String>();
		incompatibleList.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = 1L;
			String[] values = incompatibleValues.toArray(new String[0]);
			public int getSize() {return values.length;}
			public String getElementAt(int index) {return values[index];}
		});
		incompatibleScrollPane.setViewportView(incompatibleList);
		
		JLabel titleLbl = new JLabel("Informe de compatibilidad de cosm\u00E9tico");
		titleLbl.setForeground(Color.WHITE);
		titleLbl.setBackground(Color.DARK_GRAY);
		titleLbl.setOpaque(true);
		titleLbl.setFont(new Font("Dialog", Font.PLAIN, 14));
		titleLbl.setHorizontalAlignment(SwingConstants.CENTER);
		titleLbl.setBounds(0, 0, 598, 46);
		getContentPane().add(titleLbl);
		
		JLabel conditionLbl = new JLabel("Afecci\u00F3n");
		conditionLbl.setBounds(44, 57, 56, 24);
		getContentPane().add(conditionLbl);
		
		JLabel ingredientsLbl = new JLabel("Ingredientes");
		ingredientsLbl.setBounds(44, 95, 74, 14);
		getContentPane().add(ingredientsLbl);
		
		JTextField conditionTxtField = new JTextField();
		conditionTxtField.setEditable(false);
		conditionTxtField.setBounds(145, 57, 205, 20);
		conditionTxtField.setColumns(10);
		conditionTxtField.setText(compatibilityReport.getCondition());
		getContentPane().add(conditionTxtField);
			
		JTextField ingrsTxtField = new JTextField();
		ingrsTxtField.setEditable(false);
		ingrsTxtField.setColumns(10);
		ingrsTxtField.setBounds(145, 92, 426, 20);
		ingrsTxtField.setText(compatibilityReport.getIngredients().toString());
		getContentPane().add(ingrsTxtField);
		
		JLabel resultsLbl = new JLabel("Resultados");
		resultsLbl.setBackground(UIManager.getColor("Button.background"));
		resultsLbl.setOpaque(true);
		resultsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		resultsLbl.setFont(new Font("Dialog", Font.PLAIN, 12));
		resultsLbl.setBounds(0, 130, 598, 30);
		getContentPane().add(resultsLbl);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(20, 165, 551, 14);
		getContentPane().add(separator);
	}
}
