package views.form;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Label;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import controllers.SelectFileController;

public class ReportForm extends JFrame
{
	private static final long serialVersionUID = 1L;
	private Button uploadImgBtn;
	private Button generateReportBtn;
	private JTextField imgPathTxtField;
	private JFileChooser fileChooser;
	
	public ReportForm()
	{
		initialize();
	}
	
	private void initialize() 
	{
		setTitle("Meita SkinCare 1.0");
		setBounds(100, 100, 450, 272);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 43, 414, 2);
		getContentPane().add(separator);
		
		Label label = new Label("Compatibilidad de cosm\u00E9ticos");
		label.setFont(new Font("Dialog", Font.PLAIN, 12));
		label.setAlignment(Label.CENTER);
		label.setBounds(83, 10, 252, 22);
		getContentPane().add(label);
		
		JPanel panel = new JPanel();
		TitledBorder tBorder= new TitledBorder(null, "Ingredientes cosm\u00E9ticos", TitledBorder.LEADING, TitledBorder.TOP, null, null);
		tBorder.setTitleFont(new Font("Dialog", Font.PLAIN, 12));
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Etiqueta de ingredientes", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 83, 414, 84);
		panel.setOpaque(false);
		panel.setLayout(null);
		getContentPane().add(panel);
		
		uploadImgBtn = new Button("Subir imagen");
		uploadImgBtn.setBounds(10, 30, 107, 29);
		uploadImgBtn.addActionListener(new SelectFileController(this));
		panel.add(uploadImgBtn);
		
		imgPathTxtField = new JTextField();
		imgPathTxtField.setBounds(140, 30, 252, 29);
		imgPathTxtField.setColumns(10);
		imgPathTxtField.setEditable(false);
		panel.add(imgPathTxtField);
		
		generateReportBtn = new Button("Generar informe");
		generateReportBtn.setBounds(161, 183, 107, 29);
		getContentPane().add(generateReportBtn);
		
		repaint();
		
		fileChooser = new JFileChooser();
		
	}
	
	public Button getGenerateReportBtn()
	{
		return generateReportBtn;
	}

	public JTextField getImgPathTxtField()
	{
		return imgPathTxtField;
	}
	
	public JFileChooser getFileChooser()
	{
		return fileChooser;
	}
}
