package meitaSkinCareUI;

import config.MeitaSkinCare;
import controllers.ReportController;
import useCase.MakeReportUseCase;
import views.form.ReportForm;

public class MeitaSkinCareUI 
{
	public static void main(String[] args)
	{
		MeitaSkinCare app = new MeitaSkinCare();
		MakeReportUseCase makeReportUseCase = app.initializeMakeReportUseCase();
		ReportForm form = new ReportForm();
		ReportController controller = new ReportController(form,makeReportUseCase);
		
		controller.start();
	}
}
